# insomnia-plugin-jsonschema-generator

Generates a [JSON Schema](https://json-schema.org) from the request body or response.


## Requirements

[Insomnia Rest](https://insomnia.rest) Client version 2021.7.2 or above


## Generates a JSON Schema from the request body

![](https://i.imgur.com/2HA27KE.gif)


## Enable/disable JSONSchema preview from the request response.

![](https://i.imgur.com/I5Fewq8.gif)


# Set up

Go to *Insomnia > Preferences > Plugins*, type in **@homeinside/insomnia-plugin-jsonschema-generator** and click **Install Plugin.**

Alternatively, check out the manual installation.


## Manual installation

Clone this repo inside:

- **MacOS:** ~/Library/Application\ Support/Insomnia/plugins/
- **Windows:** %APPDATA%\Insomnia\plugins\
- **Linux:** $XDG_CONFIG_HOME/Insomnia/plugins/ or ~/.config/Insomnia/plugins/

Install dependencies using **npm install**


## Inspirations

 - [Create a Plugin for Insomnia Rest Client](https://docs.insomnia.rest/insomnia/introduction-to-plugins)
 - [Insomnia plugins - Hooks and Actions](https://docs.insomnia.rest/insomnia/hooks-and-actions)
 - [JSON schema](https://json-schema.org)
 - [Insomnia Plugin Hub](https://insomnia.rest/plugins)
 - [ruzicka/to-json-schema](https://github.com/ruzicka/to-json-schema)
 - [benemohamed/insomnia-plugin-json-schema](https://github.com/benemohamed/insomnia-plugin-json-schema)


## License
The full text of the license can be found in the file __MIT-LICENSE.txt__.
