/*!
 * insomnia-plugin-jsonschema-generator
 * generates a JSON Schema from the request body or response.
 *
 * more info:
 *
 * https://gitlab.com/HomeInside/insomnia-plugin-jsonschema-generator
 *
 * Copyright(c) 2022 Jorge Brunal Perez (aka Diniremix), Inc. and other contributors
 * MIT Licensed
 */

const toJsonSchema = require("to-json-schema");

// example from http://jsfiddle.net/KJQ9K/554/
function syntaxHighlight(json) {
	json = json.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
	return json.replace(
		/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
		function (match) {
			var cls = "number";
			if (/^"/.test(match)) {
				if (/:$/.test(match)) {
					cls = "key";
				} else {
					cls = "string";
				}
			} else if (/true|false/.test(match)) {
				cls = "boolean";
			} else if (/null/.test(match)) {
				cls = "null";
			}
			return '<span class="' + cls + '">' + match + "</span>";
		}
	);
}

const createDialog = (context, body, msg) => {
	const schema = toJsonSchema(JSON.parse(body));

	const newBody = JSON.stringify(schema, null, 4);

	const newBodyHighlight = syntaxHighlight(newBody);

	const div = document.createElement("div");

	const styles = document.createElement("style");
	const css =
		"pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }.string { color: green; }.number { color: darkorange; }.boolean { color: blue; }.null { color: magenta; }.key { color: red; }";
	styles.appendChild(document.createTextNode(css));

	div.appendChild(styles);

	const root = document.createElement("pre");
	root.innerHTML = newBodyHighlight;

	div.appendChild(root);

	context.app.dialog(`JSONSchema generator (${msg})`, div, {
		onHide: () => {
			context.app.alert("JSONSchema generator", "JSONSchema copied to clipboard!.");
			context.app.clipboard.writeText(newBody);
		},
	});
};

const generateActionBody = {
	label: "generate JSONSchema",
	action: async (context, data) => {
		const {request} = data;
		const method = request.method;

		if (method == "GET" || method == "DELETE") {
			context.app.alert("JSONSchema generator", "method not allowed, use on POST/PUT method.");
			return;
		} else {
			const body = request.body.text;

			createDialog(context, body, "body Preview");
		}
	},
};

const actionEnableDisableResponse = {
	label: "enable/disable JSONSchema response",
	action: async (context, data) => {
		const {request} = data;
		const existKey = await context.store.hasItem(`jsonschema.${request._id}`);

		if (existKey) {
			console.log("jsonschema-generator: response preview disabled.");
			context.store.removeItem(`jsonschema.${request._id}`);

			context.app.alert("JSONSchema generator", "response preview disabled.");
			return false;
		} else {
			console.log("jsonschema-generator: response preview enabled.");
			context.store.setItem(`jsonschema.${request._id}`, "true");

			context.app.alert("JSONSchema generator", "response preview enabled.");
		}
	},
};

const generateResponse = async (context) => {
	let body = context.response.getBody();
	const isEnabled = await context.store.hasItem(`jsonschema.${context.request.getId()}`);

	if (isEnabled == true) {
		if (body != null && body.length) {
			const bodyDecoded = new TextDecoder("utf-8").decode(body);
			createDialog(context, bodyDecoded, "response Preview");
		}
	}
};

exports.requestActions = [generateActionBody, actionEnableDisableResponse];
exports.responseHooks = [generateResponse];
